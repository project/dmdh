<?php

/**
 * Check if the current page matches with a specific pattern.
 * @param  string  $pattern The pattern to be checked. The following tokens are
 * available:
 * - %d to specify a random number
 * - %s to specify a random string
 * @param  boolean $strict  Defines if the url must match exactly or just begin
 * like the pattern.
 * @return boolean
 */
function dmdh_match_url($pattern, $strict = FALSE, $check_alias = FALSE) {
  $args = arg();
  $url = explode('/', $pattern);

  $match = _dmdh_match_url($args, $url, $strict); // Checking the original

  if (!$match && $check_alias) { // Checking the alias
    $original = $_GET['q'];
    $alias = drupal_get_path_alias();

    if ($alias != $original) {
      $args = explode('/', $alias);
      $match = _dmdh_match_url($args, $url, $strict);
    }
  }

  return $match;
}

/**
 * Determines if a user has a specific role assigned.
 * @param  string $role    The role name to be checked.
 * @param  object $account (optional) The user account to be checked. Defaults
 * to current user.
 * @return boolean
 */
function dmdh_user_has_role($role, $account = NULL) {
  if (empty($account)) {
    global $user;
    $account = user_load($user->uid);
  }
  elseif (is_numeric($account)) {
    $account = user_load($account);
  }

  if (!empty($account->roles)) {
    return in_array($role, $account->roles, TRUE);
  }

  return FALSE;
}

/**
 * Gives the contrast color (black or white) depending if the given color is
 * light or dark.
 * @param  string $hexcolor Color in hex notation (#123456).
 * @return string           Contrast color in hex notation (#FFFFFF).
 */
function dmdh_get_contrast_color($hexcolor) {
  $r = substr($hexcolor, 1, 2);
  $g = substr($hexcolor, 3, 2);
  $b = substr($hexcolor, 5, 2);

  // Y = 0.2126 R + 0.7152 G + 0.0722 B (http://en.wikipedia.org/wiki/Luminance_(relative))
  if ((0.2126 * hexdec($r) + 0.7152 * hexdec($g) + 0.0722 * hexdec($b)) < 128) {
    $contrast_color = '#FFFFFF';
  }
  else {
    $contrast_color = '#000000';
  }

  return $contrast_color;
}

/**
 * Helper function for dmdh_match_url() function. You should use that function
 * instead.
 */
function _dmdh_match_url($args, $url, $strict) {
  $match = TRUE;

  if (count($url) > count($args)) {
    $match = FALSE;
  }
  elseif ($strict && (count($args) != count($url))) {
    $match = FALSE;
  }
  else {
    foreach ($url as $key => $value) {
      switch ($value) {
        case '%d':
        $match &= is_numeric($args[$key]);
        break;
        case '%s':
        break;
        default:
        $match &= ($args[$key] == $value);
        break;
      }
    }
  }
  return $match;
}
