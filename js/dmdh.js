(function ($) {

  Drupal.behaviors.dmdh = {

    attach: function (context, settings) {

      $('body').once('dmdh', function(){

      });

      // Add target attribute to A elements.
      $("a[dmdh-target]").each(function () {
        var a_target = $(this).attr('dmdh-target');
        $(this).attr('target', a_target);
      });

      // SUMMARY WITH HIDDEN DETAIL (aka Felipe Jaramillo effect)
      $('body').dmdh_reset_swhd_boxes();
      $(window).resize(function() {
        $('body').dmdh_reset_swhd_boxes();
      });

      $('.dmdh-summary-with-hidden-detail').click(function (event) {
        var event_timestamp = event.timeStamp;
        if ($(this).attr('data_event_timestamp') != event_timestamp) {
          $(this).attr('data_event_timestamp', event_timestamp);
          var $clicked_summary = $(this);
          var dmdh_swhd_box = $clicked_summary.attr('dmdh_swhd_box');
          var box_selector = '#' + dmdh_swhd_box;
          var $box = $(box_selector);
          if ($clicked_summary.hasClass('dmdh-swhd-active')) {
            $clicked_summary.removeClass('dmdh-swhd-active');
            $box.slideUp();
            $box.removeClass('dmdh-swhd-active-box');
          }
          else {
            var $active_box = $('.dmdh-swhd-active-box');
            if ($active_box.size() > 0) {
              $active_box.slideUp(400, function () {
                $('body').dmdh_open_swhd_box($clicked_summary);
              });
            }
            else {
              $('body').dmdh_open_swhd_box($clicked_summary);
            }
          }
        }
      });

    }

  }

  $.fn.dmdh_open_swhd_box = function($clicked_summary) {
    $('.dmdh-swhd-active-box').removeClass('dmdh-swhd-active-box');
    $('.dmdh-swhd-active').removeClass('dmdh-swhd-active');

    $("html, body").animate({
      scrollTop: $clicked_summary.offset().top
    });
    $clicked_summary.addClass('dmdh-swhd-active');

    var dmdh_swhd_box = $clicked_summary.attr('dmdh_swhd_box');
    var box_selector = '#' + dmdh_swhd_box;
    var $box = $(box_selector);
    var $detail = $clicked_summary.find('.dmdh-swhd-detail');

    $box.html($detail.html());
    $box.addClass('dmdh-swhd-active-box')
    $box.slideDown();
  }

  $.fn.dmdh_reset_swhd_boxes = function() {
    $('.dmdh-swhd-active').removeClass('dmdh-swhd-active');
    $('.dmdh-swhd-first').removeClass('dmdh-swhd-first');
    $('.dmdh-swhd-last').removeClass('dmdh-swhd-last');
    $('.dmdh-swhd-box').remove();

    var box = 1;
    var max_height = 0;
    var new_row = true;
    $('.dmdh-summary-with-hidden-detail').each(function () {
      if (new_row) {
        new_row = false;
        $(this).addClass('dmdh-swhd-first');
      }
      max_height = Math.max(max_height, $(this).height());
      var box_id = 'dmdh-swhd-box-' + box;
      $(this).attr('dmdh_swhd_box', box_id);

      var add_box = false;

      var $next = $(this).next('.dmdh-summary-with-hidden-detail');

      if ($next.size() == 0) {
        add_box = true;
      }
      else if ($(this).offset().top != $next.offset().top) {
        add_box = true;
      }

      if (add_box) {
        new_row = true;
        $(this).addClass('dmdh-swhd-last');
        $(this).after('<div id="' + box_id + '" class="dmdh-swhd-box" style="display:none;"></div>');
        box++;
        $('div[dmdh_swhd_box="' + box_id + '"]').height(max_height);
        max_height = 0;
      }
    });
  }

})(jQuery);
